package TestCases;

import Initiate.BaseTest;
import PageDefintion.AboutPage;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

public class aboutTest extends BaseTest {
    @Test
    public void navigate_to_about () throws FileNotFoundException {
        AboutPage aboutPage = new AboutPage(driver);
        aboutPage.About();
    }
}
