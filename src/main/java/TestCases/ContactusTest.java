package TestCases;

import Initiate.BaseTest;
import PageDefintion.ContactusPage;
import com.sun.org.glassfish.gmbal.Description;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

public class ContactusTest extends BaseTest {
    @Test(priority = 2)
    public void contactus() throws FileNotFoundException {
        ContactusPage contactusPage = new ContactusPage(driver);
        contactusPage.contactUS();

    }

    @Test(priority = 1)
    public void empty_contactus () throws FileNotFoundException {
        ContactusPage contactusPage = new ContactusPage(driver);
        contactusPage.contactUs_empty();
    }
}
