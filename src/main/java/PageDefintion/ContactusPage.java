package PageDefintion;

import Initiate.BaseTest;
import Initiate.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.FileNotFoundException;

public class ContactusPage extends BaseTest {
    @FindBy(xpath = "//*[@id=\"contact\"]/a")
    WebElement Contact ;
    @FindBy (name = "name")
    WebElement Username;
    @FindBy(name = "phone")
    WebElement phone_number;
    @FindBy (name = "email")
    WebElement Gmail;
    @FindBy (name="msg")
    WebElement SMS ;
    @FindBy (xpath = "//*[@id=\"contactbtnsubmit\"]/span")
    WebElement submit;

    public ContactusPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void contactUS () throws FileNotFoundException {
        driver.manage().window().maximize();
        driver.navigate().to(PropertiesReader.ReadData("hosturl"));
        this.Contact.click();
        this.Gmail.sendKeys("faten@nadsoft.net");
        this.Username.sendKeys("fa10");
        this.phone_number.sendKeys("45454545445");
        this.SMS.sendKeys("26656");
        this.submit.click();
        String contact_us_meesage = driver.getPageSource();
        if (contact_us_meesage.contains("Thank you for Contact us!")){
            System.out.println(contact_us_meesage+"Contact us submitted");
        }
        else {
            System.out.println(contact_us_meesage + "Contact us not submitted");
        }
    }

    public void contactUs_empty () throws FileNotFoundException {
        driver.manage().window().maximize();
        driver.navigate().to(PropertiesReader.ReadData("hosturl"));
        this.submit.click();
        String contact_us_Message = driver.getPageSource();
        if (contact_us_Message.contains("Contact Us Form")) {
            System.out.println("contact us empty is passed");
        } else {
            System.out.println("contact us empty is failed");
        }
    }
}

