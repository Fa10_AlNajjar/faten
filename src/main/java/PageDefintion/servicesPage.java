package PageDefintion;

import Initiate.BaseTest;
import Initiate.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.FileNotFoundException;

public class servicesPage extends BaseTest {
    @FindBy(xpath = "//*[@id=\"services\"]/a")
    WebElement service ;

    public servicesPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    public void servicepage() throws FileNotFoundException {
        driver.manage().window().maximize();
        driver.navigate().to(PropertiesReader.ReadData("hosturl"+"services"));
        this.service.click();

    }}
