package PageDefintion;

import Initiate.BaseTest;
import Initiate.PropertiesReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.FileNotFoundException;

public class AboutPage extends BaseTest {
    @FindBy(xpath = "//*[@id=\"about\"]/a")
    WebElement about ;
    public AboutPage(WebDriver driver) {
        PageFactory.initElements(driver , this);
    }

    public  void About () throws FileNotFoundException {
        driver.manage().window().maximize();
        driver.navigate().to(PropertiesReader.ReadData("hosturl"+ "/about"));
        this.about.click();

    }
}
