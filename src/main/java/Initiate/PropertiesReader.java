package Initiate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

public class PropertiesReader {
    private static String Par = "";
    public static String ReadData(String par) throws FileNotFoundException {
        File file = new File("src/main/resources/config.properties");
        FileInputStream fileInput = null;
        Properties prop = new Properties();
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            System.out.println("Please create xmlfiles folder in resource and move all XML locator files into,then back to excute test cases\n" +
                    "Please check config file if exist");
        }
        try {
            prop.load(fileInput);
        } catch (Exception e) {
            System.out.println("Please check config file Inputs");
        }
        return Par = prop.getProperty(par);
    }
}
